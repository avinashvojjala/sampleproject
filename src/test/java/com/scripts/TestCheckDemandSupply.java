package com.scripts;

import static io.restassured.RestAssured.given;
import java.util.List;
import org.json.simple.JSONObject;
import io.restassured.response.Response;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.testng.annotations.Test;

public class TestCheckDemandSupply {

    @Test
    public void checkSupplyAgainstDemand() throws ParseException {
        String supplyCount = "";
        String skillsExist = "";
        String loginString = "{\"username\":\"admin\",\"password\":\"admin@edge!$#\"}";

        String supplyString = "{\"show_only\":\"unacted\",\"filters\":[],\"query\":\"\",\"result\":1,\"min_score\":10,\"show_updated\":false,\"toggles\":[],\"page\":1}";
        JSONParser parser = new JSONParser();
        JSONObject loginJson = (JSONObject) parser.parse(loginString);
        JSONObject supplyJson = (JSONObject) parser.parse(supplyString);
        //given().header("Accept", "application/json").header("Content-Type", "application/json").when().body(loginJson).post("https://ntt--uat--ha-iwo.hirealchemy.com/authenticate");
        String accessToken = given().header("Accept", "application/json").header("Content-Type", "application/json").when().body(loginJson).post("https://ntt--uat--ha-iwo.hirealchemy.com//authenticate").then().extract().response().jsonPath().get("token");
        System.out.println(accessToken);
        boolean demandsExists = true;
        int i = 1;
        int statusCode = 200;
        String getDemands = "{\"show_only\":\"\",\"sort_by\":\"billing_start_date\",\"sort_order\":\"desc\",\"show_updated\":true,\"filters\":[{\"property_name\":\"demand_status\",\"property_value\":[\"Reviewed by Resourcing Teams\",\"Open for Hiring\",\"Proposal � Open for Hiring\",\"To Close\",\"Proposal � To Close\",\"Hold\"]}],\"query\":\"\",\"range_filter\":[],\"default\":1,\"result\":1}";

        JSONObject json = (JSONObject) parser.parse(getDemands);
        while (demandsExists) {
            Response response = given().header("X-Access-Token", accessToken).when().body(json).post("https://ntt--uat--ha-iwo.hirealchemy.com/demands/search/" + i);
            statusCode = response.getStatusCode();

            if (!(statusCode == 200)) {
                demandsExists = false;
            } else {
                i = i + 1;
                List<String> demandIds = response.jsonPath().getList("demand_results.demand_id"); //get("demand_results.demand_id");
                for (String id : demandIds) {
                    Response supplyResponse = given().header("X-Access-Token", accessToken).header("Accept","application/json").header("Content-Type","application/json").when().body(supplyJson).post("https://ntt--uat--ha-iwo.hirealchemy.com//demands/" + id + "/supplies");

                    if (supplyResponse.getStatusCode() == 200) {
                        try {
//                            if (supplyResponse.jsonPath().get("total").equals(null)) {
//                                supplyCount = "0";
//                            } else {
                                supplyCount = supplyResponse.jsonPath().get("total").toString();

//                            }

                            if (supplyResponse.jsonPath().get("skills").toString().length() > 2) {

                                skillsExist = "true";
                            } else {
                                skillsExist = "false";

                            }
                            System.out.println(id + "^" + supplyCount + "^" + skillsExist);
                        } catch (Exception e) {
                            System.out.println("https://ntt--qa--iwo-webapp.hirealchemy.com/demands/" + id + "/supplies");
                        }
                    }
                    else{
                        System.out.println(id + "^"+"FAILURE"+"^"+supplyResponse.getStatusCode());
                    }

                }
            }
        }


    }

}