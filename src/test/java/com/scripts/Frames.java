package com.scripts;

import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import com.base.TestBase;

public class Frames extends TestBase {

	@BeforeClass
	public static void startTest() {
		
		initialization(); 
	}
	
	@Test
	public void iFrames() {
		List<WebElement> ele=driver.findElements(By.tagName("iframe"));
		
		System.out.println("The number of frames in the page is:"+ele.size());
		driver.switchTo().frame(0);
		System.out.println("I am in the frame ");
		driver.switchTo().defaultContent();
		System.out.println("I am out of the frame ");
		driver.close();
		
	}
	
}
